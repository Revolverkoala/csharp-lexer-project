﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LibYToy
{
    // A Grammar is:
    // A list of final states, after which we should transition back to 0 without reading a character
    // an alphabet (valid characters)
    // a transition matrix
    
    // INVARIANT: Alphabet MUST have the same extent as the transition matrix's second rank
    // i.e. the number of columns need to match
    public class Grammar
    {
        public char[] Alphabet { get; private set; }
        public int [,] Matrix { get; private set; }
        public List<FinalState> FinalStates { get; private set; }

        public Grammar(IEnumerable<char> alphabet, int[,] matrix, IEnumerable<FinalState> finalStates)
        {
            //If alphabet is already a char[] we can cast it, otherwise make it into one
            if (alphabet.GetType() == typeof(char[]))
                Alphabet = (char[]) alphabet;
            else
                Alphabet = alphabet.ToArray();

            //If the alphabet isn't the same length as the number of columns in the matrix
            //that's Very Bad so throw an exception if they don't match
            if (Alphabet.Length != matrix.GetLength(1))
                throw new ArgumentException("alphabet");

            Matrix = matrix;

            if (finalStates.GetType() == typeof(List<FinalState>))
                FinalStates = (List<FinalState>) finalStates;
            else
                FinalStates = finalStates.ToList();
        }

        public Grammar(IEnumerable<char> alphabet, int[,] matrix, IEnumerable<Tuple<int, string>> finalStates)
            : this(alphabet, matrix, FinalState.FromTuples(finalStates)) { }

        public Grammar(IEnumerable<char> alphabet, int[,] matrix, IEnumerable<Tuple<string, int>> finalStates)
            : this(alphabet, matrix, FinalState.FromTuples(finalStates)) { }

        public class FinalState
        {
            public int StateId { get; private set; }
            public string Description { get; private set; }

            public FinalState(int id, string desc)
            {
                StateId = id;
                Description = desc;
            }

            public static List<FinalState> FromTuples(IEnumerable<Tuple<int, string>> tups)
            {
                //Give Select a method as an argument and it'll apply it to each thing
                return tups.Select(FromTuple).ToList();
            }

            private static FinalState FromTuple(Tuple<int, string> tup)
            {
                return new FinalState(tup.Item1, tup.Item2);
            }

            public static List<FinalState> FromTuples(IEnumerable<Tuple<string, int>> tups)
            {
                return tups.Select(FromTuple).ToList();
            }

            private static FinalState FromTuple(Tuple<string, int> tup)
            {
                return new FinalState(tup.Item2, tup.Item1);
            }
        }
    }
}
