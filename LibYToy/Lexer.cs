﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
// ReSharper disable CheckNamespace

namespace LibYToy
{
    public class Lexer
    {
        public Grammar Grammar { get; private set; }

        public Lexer(Grammar g)
        {
            Grammar = g;
        }

        public Lexer(IEnumerable<char> alphabet, int[,] matrix, IEnumerable<Grammar.FinalState> finalStates)
            : this(new Grammar(alphabet, matrix, finalStates)) { }

        //public LexerToken[] TokenizeFile(string filename)
        //{

        //}
    }
}
