﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibYToy
{
    public class LexerToken
    {
        public string TokenString { get; private set; }
        public string TokenType { get; private set; }

        public LexerToken(string tokenStr, string tokenType)
        {
            TokenString = tokenStr;
            TokenType = tokenType;
        }
    }
}
