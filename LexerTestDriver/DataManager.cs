﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibYToy;

namespace LexerTestDriver
{
    class DataManager
    {
        //Constants (defined by actual matrix)
        const int DEFAULT_ROWS = 101;
        const int DEFAULT_COLS = 95;
        const int INITIAL_STATE = 0;
        const int ERROR_STATE = 100;

        private static readonly char[] DEFAULT_ALPHABET = {' ', '!',  '"',	'#', '$', '%',	'&', '\'', '(', ')', '*', '+', ',', '-', '.', '/', '0', '1', '2', '3'
                , '4', '5', '6', '7', '8', '9', ':', ';', '<', '=', '>', '?', '@', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M'
                , 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '[', '\\', ']', '^', '_', '`', 'a', 'b', 'c', 'd', 'e', 'f', 'g'
                , 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '{', '|', '}', '~'} ;

        //Copy the array on output to keep it from being changed
        //public static char[] DefaultAlphabet => (char[])DEFAULT_ALPHABET.Clone();
        public static char[] DefaultAlphabet { get { return (char[])DEFAULT_ALPHABET.Clone(); } }

        //Cell range C9:CS109
        //101 rows, 95 columns
        // int[101,95]

            //Hard coding this for now
            /// <summary>
            /// Read raw transition matrix state data from a CSV file.
            /// This is somewhat brittle, but it should reject (or truncate, I forget how C#'s parser works) non-integers.
            /// WARNING: The CSV ABSOLUTELY MUST have the correct number of columns in EVERY row or the result will be valid but HIGHLY INCORRECT.
            /// You can leave off trailing empty (i.e. error-only) rows, though. I'll come up with a better format later.
            /// </summary>
            /// <param name="filename">Path to the file to open</param>
            /// <param name="rows"># of rows in matrix</param>
            /// <param name="cols"># of columns in matrix</param>
            /// <param name="errorState">Error state (default to fill matrix with, since most values will be this)</param>
            /// <returns>A 2d array containing the transition matrix state grid</returns>
        public static int[,] ReadMatrixFromCsv(string filename, int rows = DEFAULT_ROWS, int cols = DEFAULT_COLS, int errorState = ERROR_STATE)
        {
            //Fill matrix with error state
            var matrix = InitMatrix(rows, cols, errorState);
            var cells = new List<string>();

            //Open input file
            using (var file = new StreamReader(filename))
            {
                string line;

                //Since we're traversing the array in the same direction we traverse the file,
                //let's flatten our matrix into a queue for a moment
                while ((line = file.ReadLine()) != null)
                {
                    //Split each line into cells and add it directly to the 
                    cells.AddRange(line.Split(new[] { ',' }, StringSplitOptions.None));
                }
            }

            //lists have AddRange() but queues don't have EnqueueRange()
            var cellQ = new Queue<string>(cells);

            
            for (int row = 0; row < matrix.GetLength(0); ++row)
            {
                for (int col = 0; col < matrix.GetLength(1); ++col)
                {
                    string cell;

                    //If the queue is empty that means there's nothing else of interest
                    //We already filled the matrix with the error value so we can just stop iterating
                    if (cellQ.Count > 0)
                        cell = cellQ.Dequeue();
                    else
                        break;
                        
                    
                    int cellVal;

                    //Leave the default value there if we don't have an integer
                    if (!string.IsNullOrWhiteSpace(cell) && int.TryParse(cell, out cellVal))
                        matrix[row, col] = cellVal;
                }

                //No point traversing another row if we're out of new values to set
                if (cellQ.Count == 0)
                    break;
            }

            return matrix;
        }

        private static int[,] InitMatrix(int rows, int cols, int errorState)
        {
            var matrix = new int[rows, cols];

            for (int row = 0; row < matrix.GetLength(0); ++row)
            {
                for (int col = 0; col < matrix.GetLength(1); ++col)
                {
                    matrix[row, col] = errorState;
                }
            }

            return matrix;
        }

        //Int comes first
        public static List<Grammar.FinalState> ReadFinalStates(string filename)
        {
            var states = new List<Grammar.FinalState>();

            using (var file = new StreamReader(filename))
            {
                string line;

                while ((line = file.ReadLine()) != null)
                {
                    var splitStr = line.Split(',');
                    int stateNum;

                    if (splitStr.Length == 2 && int.TryParse(splitStr[1], out stateNum))
                        states.Add(new Grammar.FinalState(stateNum, splitStr[0]));
                }
            }

            return states;
        }

        public static char[] ReadAlphabet(string filename)
        {
            char[] alphabet;

            using (var file = new StreamReader(filename))
            {
                alphabet = file.ReadToEnd().ToCharArray();
            }

            return alphabet;
        }
    }
}
