﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibYToy;

namespace LexerTestDriver
{
    class Program
    {
        static void Main(string[] args)
        {
            //Test reading the transition matrix from a csv
            var matrix = DataManager.ReadMatrixFromCsv("data/matrix.csv");

            //Write that mess to a file so I can look at it in a sane environment
            using (var fs = new FileStream("output.txt", FileMode.Create))
            using (var file = new StreamWriter(fs))
            {
                file.Write(Int2dArray2Str(matrix));
            }

            //Read final states from file
            var finalStates = DataManager.ReadFinalStates("data/final-states.txt");

            //TODO: Make sure the result of this is exactly the same as the default alphabet array in DataManager
            var alphabet = DataManager.ReadAlphabet("data/alphabet.txt");

            //Create a Grammar object with this data (and the alphabet hard-coded into DataManager)
            var grammar = new Grammar(alphabet, matrix, finalStates);

            //That should have thrown an exception if anything went drastically wrong
            Console.WriteLine(grammar.Alphabet);

            

            ConsolePause("Done. Press any key to continue...");
        }

        static string Int2dArray2Str(int[,] arr)
        {
            const string padding = " | ";
            var sb = new StringBuilder();

            for (int row = 0; row < arr.GetLength(0); ++row)
            {
                for (int col = 0; col < arr.GetLength(1); ++col)
                {
                    //newline if we're at the end, otherwise padding
                    sb.Append(arr[row, col] + (col == arr.GetLength(1) - 1 ? "\n" : padding));
                }
            }

            return sb.ToString();
        }

        static void ConsolePause(string str)
        {
            Console.WriteLine(str);
            Console.ReadKey();
        }
    }
}
